# ssh_deploy_key

Ansible role for copying SSH Deploy Key to hosts

## Requirements

* Ansible >= 1.9

## Role Variables

Private SSH deploy key on local control machine. This can be encrypted with ansible-vault if you do not wish to expose the file name in plain text.

    ssh_deploy_key_file: ~/.ssh/your_deploy_key_id_rsa

Destination directory on target host

    ssh_deploy_key_dest: ~/.ssh/

## Dependencies

None.

## Example Playbook

    - name: Copy SSH Deploy Key
      import_role:
        name: ssh_deploy_key

## License

The MIT License (MIT)

Copyright (c) 2018 Matthew C. Veno

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

https://opensource.org/licenses/MIT

## Author Information

Matthew C. Veno
